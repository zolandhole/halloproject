<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templates extends MX_Controller {
function __construct() {
	parent::__construct();
}

	function admin($data){
		if (!isset($data['view_module'])) {
			$data['view_module'] = $this->uri->segment(1);
		}
		if (!isset($data['view_file'])) {
			$data['view_file'] = $this->uri->segment(2);
		}
		$this->load->view('admin', $data);
	}

	function website($data){
		if (!isset($data['view_module'])) {
			$data['view_module'] = $this->uri->segment(1);
		}
		if (!isset($data['view_file'])) {
			$data['view_file'] = $this->uri->segment(2);
		}
		$this->load->view('website', $data);
	}

}
