<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_categories extends MX_Controller {
function __construct() {
	parent::__construct();
	echo modules::run('Site_security/_make_sure_is_admin');
}

	function manage()
	{
		$parent_cat_id = $this->uri->segment(3);
		if (!is_numeric($parent_cat_id)) {
			$parent_cat_id = 0;
		}
		$data['parent_cat_id'] = $parent_cat_id;
		$data['flash'] = $this->session->flashdata('item');
		$data['query'] = $this->_get_where_custom('parent_cat_id', $parent_cat_id);
		$data['headline'] = 'Manage Categories';
		echo modules::run('Templates/admin',$data);
	}

	function create(){
		$this->load->library('session');

		$update_id = $this->uri->segment(3);
		$submit = $this->input->post('submit', TRUE);

		if ($submit == 'Cancel') {
			redirect('Store_categories/manage');	
		} elseif ($submit == 'Submit') {

			$this->load->library('form_validation');
			$this->form_validation->CI =& $this;
			$this->form_validation->set_rules('category_title','Category Title','required|max_length[150]');

			if ($this->form_validation->run() == TRUE) {
				$data = $this->_fetch_data_from_post();
				if (is_numeric($update_id)) {
					$this->_update($update_id, $data);
					$flash_msg = "The Category details were successfully updated.";
					$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
					$this->session->set_flashdata('item', $value);
				} else {
					$this->_insert($data);
					$update_id = $this->_get_max();
					$flash_msg = "The Category was successfully added.";
					$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
					$this->session->set_flashdata('item', $value);
				}
				redirect('Store_categories/create/'.$update_id);
			}
		}

		if (!isset($update_id)) {
			$data = $this->_fetch_data_from_post();
			$data['headline'] = 'Add New Category';
		}else{
			$data = $this->_fetch_data_from_db($update_id);
			if (!is_numeric($update_id)) {
				redirect('Store_categories/manage');	
			}
			if ($data == '') {
				redirect('Store_categories/manage');
			}
			$data['headline'] = 'Update Category';
		}
		$data['options'] = $this->_get_dropdown_options($update_id);
		$data['num_dropdown_options'] = count($data['options']);
		$data['update_id'] = $update_id;
		$data['flash'] = $this->session->flashdata('item');
		echo modules::run('Templates/admin',$data);
	}

function _get($order_by) 
{$this->load->model('Mdl_store_categories'); $query = $this->Mdl_store_categories->get($order_by); return $query;}
function _get_limit($limit, $offset, $order_by) 
{$this->load->model('Mdl_store_categories'); $query = $this->Mdl_store_categories->get_with_limit($limit, $offset, $order_by); return $query;}
function _get_where($id) 
{$this->load->model('Mdl_store_categories'); $query = $this->Mdl_store_categories->get_where($id); return $query;}
function _get_where_custom($col, $value) 
{$this->load->model('Mdl_store_categories'); $query = $this->Mdl_store_categories->get_where_custom($col, $value); return $query;}
function _insert($data) 
{$this->load->model('Mdl_store_categories'); $this->Mdl_store_categories->_insert($data);}
function _update($id, $data) 
{$this->load->model('Mdl_store_categories'); $this->Mdl_store_categories->_update($id, $data);}
function _delete($id) 
{$this->load->model('Mdl_store_categories'); $this->Mdl_store_categories->_delete($id);}
function _count_where($column, $value) 
{$this->load->model('Mdl_store_categories'); $count = $this->Mdl_store_categories->count_where($column, $value); return $count;}
function _get_max() 
{$this->load->model('Mdl_store_categories'); $max_id = $this->Mdl_store_categories->get_max(); return $max_id;}
function _custom_query($mysql_query) 
{$this->load->model('Mdl_store_categories'); $query = $this->Mdl_store_categories->_custom_query($mysql_query); return $query;}

	function _fetch_data_from_post(){
		$data['category_title'] = $this->input->post('category_title', TRUE);
		$data['parent_cat_id'] = $this->input->post('parent_cat_id', TRUE);
		return $data;
	}

	function _fetch_data_from_db($update_id){
		$query = $this->_get_where($update_id);
		foreach ($query->result() as $row) {
			$data['category_title'] = $row->category_title;
			$data['parent_cat_id'] = $row->parent_cat_id;
		}
		if (!isset($data)) {
			$data = '';
		}
		return $data;
	}

	function _get_dropdown_options($update_id){
		if (!isset($update_id)) {
			$update_id = 0;
		}

		$options[''] = 'Parent';
		$mysql_query = "select * from store_category where parent_cat_id=0 and id!='$update_id'";
		$query = $this->_custom_query($mysql_query);
		foreach ($query->result() as $row) {
			$options[$row->id] = $row->category_title;
		}
		return $options;
	}

	function _get_cat_title($update_id){
		$data = $this->_fetch_data_from_db($update_id);
		$cat_title = $data['category_title'];
		return $cat_title;
	}

	function _counts_sub_cat($update_id){
		$query = $this->_get_where_custom('parent_cat_id', $update_id);
		$num_rows = $query->num_rows();
		return $num_rows;
	}

	function _draw_sortable_list($parent_cat_id){
		$mysql_query = "select * from store_category where parent_cat_id='$parent_cat_id' order by priority";
		$data['query'] = $this->_custom_query($mysql_query);
		$this->load->view('sortable_list', $data);
	}

	function sort(){
		$number = $this->input->post('number', TRUE);
		for ($i=1; $i<=$number; $i++) { 
			$update_id = $_POST('order'.$i);
			$data['priority'] = $i;
			$this->_update($update_id, $data);
		}
	}
}
