<style type="text/css">
	.sort{
		list-style: none;
		border: 1px #aaa solid;
		padding: 10px;
		width: 50%;
		margin-bottom: 4px;
	}
</style>
<ul id="sortlist">
	<?php 
      $this->load->module('store_categories');
      foreach ($query->result() as $row) :
      $count_sub_cat = $this->store_categories->_counts_sub_cat($row->id);
      if ($count_sub_cat == 0) {
        $count_sub_cat = '-';
        $entity = '';
      }elseif ($count_sub_cat == 1) {
        $entity = 'Sub category';
      }else{
        $entity = 'Sub categories';
      }
      if ($row->parent_cat_id == 0) {
        $parent_cat_title = '-';
      }else{
        $parent_cat_title = $this->store_categories->_get_cat_title($row->parent_cat_id);
      }
    ?>
		<li class="sort">
			<i class="fa fa-sort"></i> &nbsp;<?=$row->category_title?>
			<?php if ($count_sub_cat != 0) : ?>
	            <div class="btn-group pull-right">
	              <a href="<?=base_url()?>Store_categories/manage/<?=$row->id?>"><button type="button" class="btn btn-success btn-xs"><i class="fa fa-eye"></i> <?=$count_sub_cat?> <?=$entity?></button></a>
	              <a href="<?=base_url()?>Store_categories/create/<?=$row->id?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button></a>
	            </div>
	        <?php endif; ?>
		</li>
	<?php endforeach;?>
</ul>