<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$headline?>
      </h1>
    </section>

  <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="box-body">
              <?=validation_errors('<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>','</div>')?>
                <?php 
                  if (isset($flash)) {
                    echo $flash;
                  }
                ?>
            </div>
      </div>
      <div class="row">
        <!-- right column -->
        <div class="col-md-7">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border" align="center">
              <?php if (!isset($update_id)): ?>
                Please fill the form to add new category
              <?php endif; ?>
              <?php if (isset($update_id)) : ?>
                <a href="<?=base_url()?>Store_categories/create">
                    <button type="button" class="btn bg-olive margin btn-xs"><i class="fa fa-fw fa-plus-square"></i> Add more Category</button>
                </a>
                <?php endif; ?>
            </div>
            <!-- /.box-header -->
            
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="<?=base_url()?>Store_categories/create/<?=$update_id?>">
            	<?php if ($num_dropdown_options > 1) { ?>
		          <div class="box-body">
	                <div class="form-group">
	                  <label class="col-sm-3 control-label">Parent Category</label>
	                  <div class="col-sm-7">
	                  	<?php 
	                  	$attributes = array('class' => 'form-control select2', 'name' => 'parent_cat_id');
	                  	echo form_dropdown('parent_cat_id', $options, $parent_cat_id, $attributes); 
	                  	?>
	                  </div>
	                </div>
	              </div>
	          	<?php } else {
	          		echo form_hidden('parent_cat_id', 0);
	          	}?>
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Category title <span style="color: red">*</span></label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control input-sm" name="category_title" value="<?=$category_title?>">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right" name="submit" value="Submit">Submit</button>
                <button type="submit" class="btn btn-default pull-1" name="submit" value="Cancel">Back</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </section>