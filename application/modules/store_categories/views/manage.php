<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$headline?>
        <small>you can manage categories here</small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="box-body">
              <?=validation_errors('<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>','</div>')?>
                <?php 
                  if (isset($flash)) {
                    echo $flash;
                  }
                ?>
            </div>
      </div>
      <!-- Default box -->
      <div class="box">
            <div class="box-header">
              	<a href="<?=base_url()?>Store_categories/create">
              		<button type="button" class="btn bg-olive margin"><i class="fa fa-fw fa-plus-square"></i>  Add Category</button>
              	</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?= modules::run('store_categories/_draw_sortable_list', $parent_cat_id) ?>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->