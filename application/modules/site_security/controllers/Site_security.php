<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_security extends MX_Controller {
function __construct() {
	parent::__construct();
}

	function _hashed($str){
		$hashed_string = password_hash($str, PASSWORD_BCRYPT, array('cost' => 4));
		return $hashed_string;
	}

	function _unhash($plain_text_str, $hashed_string){
		$result = password_verify($plain_text_str, $hashed_string);
		return $result;
	}

	function _make_sure_is_admin(){
		$is_admin = TRUE;
		if ($is_admin != TRUE) {
			redirect('Site_security/not_allowed');
		}
	}

	function not_allowed(){
		echo "you are not allowed to be here";
	}

}
