<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timedate extends MX_Controller {
function __construct() {
	parent::__construct();
}

	function _get_style($timestamp, $format){
		switch ($format) {
			case 'full':
				# Friday 18th of February 2018 at 10:00:00 AM
				$thedate = date('l jS \of F Y \a\t h:i:s A', $timestamp);
				break;
			case 'cool':
				# Friday 18th of February 2018
				$thedate = date('l jS \of F Y', $timestamp);
				break;
			case 'shorter':
				# 18th of February 2018
				$thedate = date('jS \of F Y', $timestamp);
				break;
			case 'mini':
				# 18th Feb 2018
				$thedate = date('jS M Y', $timestamp);
				break;
			case 'oldschool':
				# 11/02/18
				$thedate = date('j\/n\/y', $timestamp);
				break;
			case 'datepicker':
				# 18/2/11
				$thedate = date('d\-m\-y', $timestamp);
				break;
			case 'monyear':
				# Friday 18th Feb 2018
				$thedate = date('F Y', $timestamp);
				break;
			case 'yarud':
				# Friday 18th Feb 2018
				$thedate = date('l F Y h:i:s A', $timestamp);
				break;
			case 'mutiara':
				# Friday 18th Feb 2018
				$thedate = date('jS M y h:i', $timestamp);
				break;
		}
		return $thedate;
	}

	function _make_timestamp_datepicker($datepicker){
		$hour = 7;
		$minute = 0;
		$second = 0;

		$day = substr($datepicker, 0,2);
		$month = substr($datepicker, 3,2);
		$year = substr($datepicker, 6,4);

		$timestamp = $this->maketime($hour, $minute, $second, $month, $day, $year);
		return $timestamp;
	}

	function make_timestamp($day, $month, $year){
		$hour = 7;
		$minute = 0;
		$second = 0;
		$timestamp = $this->maketime($hour, $minute, $second, $month, $day, $year);
		return $timestamp;
	}
}
