<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_item_colors extends MX_Controller {
function __construct() {
	parent::__construct();
	echo modules::run('Site_security/_make_sure_is_admin');
}

	function test(){
		echo "string";
	}

	function update($update_id)
	{
		if (!isset($update_id)) {
			redirect('Store_items/manage');
		}
		$this->load->module('store_items');
		$data = $this->store_items->_fetch_data_from_db($update_id);
		$data['query'] = $this->_get_where_custom('item_id', $update_id);
		$data['num_rows'] = $data['query']->num_rows();
		$data['update_id'] = $update_id;
		$data['flash'] = $this->session->flashdata('item');
		$data['headline'] = 'Update Item colors';
		echo modules::run('Templates/admin',$data);
	}

	function submit($update_id)
	{
		if (!isset($update_id)) {
			redirect('Store_items/manage');
		}
		$color = trim($this->input->post('color', TRUE));
		if ($color!='') {
			$data['item_id'] = $update_id;
			$data['color'] = $color;
			$this->_insert($data);
			$flash_msg = "The Item color was successfully added.";
			$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
			$this->session->set_flashdata('item', $value);
		}
		redirect('Store_item_colors/update/'.$update_id);
	}

	function delete($update_id){
		if (!isset($update_id)) {
			redirect('Store_items/manage');
		}
		$query = $this->_get_where($update_id);
		foreach ($query->result() as $row) {
			$item_id = $row->item_id;
		}
		$this->_delete($update_id);
		$flash_msg = "The color option was successfully deleted.";
		$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
		$this->session->set_flashdata('item', $value);
		redirect('Store_item_colors/update/'.$item_id);
	}

	function _delete_item($update_id){
		$mysql_query = "delete from Store_item_colors where item_id='$update_id'";
		$this->_custom_query($mysql_query);
	}


function _get($order_by) 
{$this->load->model('Mdl_store_item_colors'); $query = $this->Mdl_store_item_colors->get($order_by); return $query;}
function _get_limit($limit, $offset, $order_by) 
{$this->load->model('Mdl_store_item_colors'); $query = $this->Mdl_store_item_colors->get_with_limit($limit, $offset, $order_by); return $query;}
function _get_where($id) 
{$this->load->model('Mdl_store_item_colors'); $query = $this->Mdl_store_item_colors->get_where($id); return $query;}
function _get_where_custom($col, $value) 
{$this->load->model('Mdl_store_item_colors'); $query = $this->Mdl_store_item_colors->get_where_custom($col, $value); return $query;}
function _insert($data) 
{$this->load->model('Mdl_store_item_colors'); $this->Mdl_store_item_colors->_insert($data);}
function _update($id, $data) 
{$this->load->model('Mdl_store_item_colors'); $this->Mdl_store_item_colors->_update($id, $data);}
function _delete($id) 
{$this->load->model('Mdl_store_item_colors'); $this->Mdl_store_item_colors->_delete($id);}
function _count_where($column, $value) 
{$this->load->model('Mdl_store_item_colors'); $count = $this->Mdl_store_item_colors->count_where($column, $value); return $count;}
function _get_max() 
{$this->load->model('Mdl_store_item_colors'); $max_id = $this->Mdl_store_item_colors->get_max(); return $max_id;}
function _custom_query($mysql_query) 
{$this->load->model('Mdl_store_item_colors'); $query = $this->Mdl_store_item_colors->_custom_query($mysql_query); return $query;}

}
