<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$headline?>
      </h1>
    </section>

      <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="box-body">
              <?=validation_errors('<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>','</div>')?>
                <?php 
                  if (isset($flash)) {
                    echo $flash;
                  }
                ?>
            </div>
      </div>
      <div class="row">
        <div class="col-md-7">
	        	<div class="box box-widget widget-user-2">

	            <!-- Add the bg color to the header using any of the bg-* classes -->
	            <div class="widget-user-header bg-purple">
	            	<div class="widget-user-image">
		                <img class="img-circle" src="<?=base_url()?>adminplugins/dist/img/size.png" alt="User Avatar">
		            </div>
		            <h3 class="widget-user-username"><?=$item_title?></h3>
		            <h5 class="widget-user-desc">add item sizes</h5>
	            </div>
	            <div class="box-footer no-padding">
	            	<form class="form-horizontal" method="POST" action="<?=base_url()?>Store_item_sizes/submit/<?=$update_id?>">
		              <div class="input-group margin">
		                <input type="text" class="form-control" name="size">
		                    <span class="input-group-btn">
		                      <button type="submit" class="btn btn-info btn-flat">Submit!</button>
		                    </span>
		              </div>
		          	</form>
	              <ul class="nav nav-stacked">
	                <li>
	                	<a href="<?=base_url()?>Store_items/create/<?=$update_id?>">Click Here to finised update item sizes!
	                		<span class="pull-right badge bg-green">
	                			<i class="fa fa-hand-o-left"></i>
	                		</span>
	                	</a>
	                </li>
	            </div>
	          </div>
	        </div>
	      
      	<div class="col-md-5">
      		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Existing Sizes</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
                <tbody>
                	<tr>
	                  <th style="width: 10px">#</th>
	                  <th>Size</th>
	                  <th class="pull-right">Action</th>
	                </tr>
	                <?php 
		                $no = 0;
		                foreach ($query->result() as $row): 
	                	$no++;
	                ?>
				        <tr>
				          <td><?=$no?></td>
				          <td><?=$row->size?></td>
				          <td>
				          	<a class="pull-right" href="<?=base_url()?>Store_item_sizes/delete/<?=$row->id?>">
			                  <button type="button" class="btn btn-danger btn-xs">
			                  	<i class="fa fa-fw fa-trash"></i>
			                  </button>
			                </a>
				          </td>
				        </tr>
				    <?php endforeach;?>
	              </tbody></table>
	            </div>
	            <!-- /.box-body -->
	          </div>
	      	</div>
	      	</div>
	      <!-- /.row -->
	    </section>
	    <!-- /.content -->