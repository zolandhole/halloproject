<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$headline?>
        <small>you can manage all your items here</small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="box-body">
              <?=validation_errors('<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>','</div>')?>
                <?php 
                  if (isset($flash)) {
                    echo $flash;
                  }
                ?>
            </div>
      </div>
      <!-- Default box -->
      <div class="box">
            <div class="box-header">
              	<a href="<?=base_url()?>Store_items/create">
              		<button type="button" class="btn bg-olive margin"><i class="fa fa-fw fa-plus-square"></i>  Add Item</button>
              	</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Item</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($query->result() as $row) : ?>
	                <tr>
	                  <td><?=$row->item_title?></td>
	                  <td align="right">Rp <?=number_format($row->item_price/1000)?> K</td>
	                  <td>
	                  		<a class="text-<?=(($row->status == 0)?'red':'green')?>"><?=(($row->status == 0)?'Inactive':'Active')?></a>
	                  </td>
	                  <td align="right">
	                  	<div class="btn-group">
                        <a href="<?=base_url()?>Store_items/create/<?=$row->id?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-search-plus"></i></button></a>
                      </div>
                      <div class="btn-group">
	                      <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal<?=$row->id?>">
                          <i class="fa fa-trash-o"></i>
                        </button>
                              <div class="modal modal-danger fade" id="modal<?=$row->id?>" align="center">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Delete Confirmation</h4>
                                    </div>
                                    <div class="modal-body">
                                      <strong>Are you sure want to delete this Item ?</strong>
                                      <p>This Item may have content image, colours, and sizez. click yes button will delete all content</p>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                      <a href="<?=base_url()?>Store_items/delete/<?=$row->id?>">
                                        <button type="button" class="btn btn-outline">Yes</button>
                                      </a>
                                    </div>
                                  </div>
                                  <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                              </div>
                      </div>
	                  </td>
	                </tr>
	            <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Item</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->