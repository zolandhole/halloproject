<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$headline?>
      </h1>
    </section>

  <!-- Main content -->
	<section class="content">
      <div class="row">
        <div class="box-body">
              <?=validation_errors('<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>','</div>')?>
                <?php 
                  if (isset($flash)) {
                    echo $flash;
                  }
                ?>
            </div>
      </div>
      <div class="row">
        <!-- right column -->
        <div class="col-md-7">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border" align="center">
              <?php if (!isset($update_id)): ?>
                Please fill the form to add new item
              <?php endif; ?>
              <?php if (isset($update_id)) : ?>
                <a href="<?=base_url()?>Store_items/create">
                    <button type="button" class="btn bg-olive margin btn-xs"><i class="fa fa-fw fa-plus-square"></i> Add more Item</button>
                </a>|
                <?php if ($item_image=='') : ?>
                  <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modal-upload_image">
                    <i class="fa fa-fw  fa-file-image-o "></i> Upload Image
                  </button>
                <?php endif; ?>
                <a href="<?=base_url()?>Store_item_colors/update/<?=$update_id?>">
                  <button type="button" class="btn btn-info btn-xs"><i class="fa fa-fw fa-paint-brush"></i> Update Colours</button>
                </a>
                <a href="<?=base_url()?>Store_item_sizes/update/<?=$update_id?>">
                  <button type="button" class="btn btn-success btn-xs"><i class="fa fa-fw fa-expand"></i> Update Sizes</button>
                </a>
                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-warning">
                  <i class="fa fa-fw fa-windows"></i>Update Item Categories
                </button>
                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-danger">
                  <i class="fa fa-fw fa-trash-o"></i>Delete Item
                </button>
                <a href="<?=base_url()?>Store_items/view/<?=$update_id?>">
                    <button type="button" class="btn badge-default margin btn-xs"><i class="fa fa-fw fa-globe"></i> View item on Shop</button>
                </a>
                <?php if ($item_image=='') : ?>
                  <div class="modal fade" id="modal-upload_image">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Upload Image</h4>
                        </div>
                        <div class="modal-body">
                          
                            <?=((isset($error))?$error:'')?>
                            <form role="form" action="<?=base_url()?>Store_items/do_upload/<?=$update_id?>" method="post" enctype="multipart/form-data">
                              <div class="box-body">
                                <div class="form-group">
                                  <label for="exampleInputFile">File input</label>
                                  <input type="file" id="exampleInputFile" name="userfile" accept=".gif,.jpg,.png,.jpeg">
                                </div>
                              </div>
                              <!-- /.box-body -->

                              <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                  <?php endif; ?>
                  <!-- /.modal -->

                  <div class="modal modal-warning fade" id="modal-warning">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Warning Modal</h4>
                        </div>
                        <div class="modal-body">
                          <p>One fine body&hellip;</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-outline">Save changes</button>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->

                  <div class="modal modal-danger fade" id="modal-danger">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Delete Confirmation</h4>
                        </div>
                        <div class="modal-body">
                          <strong>Are you sure want to delete this Item ?</strong>
                          <p>This Item may have content image, colors, and sizez. click yes button will delete all content</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                          <a href="<?=base_url()?>Store_items/delete/<?=$update_id?>">
                            <button type="button" class="btn btn-outline">Yes</button>
                          </a>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                <?php endif; ?>
            </div>
            <!-- /.box-header -->
            
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="<?=base_url()?>Store_items/create/<?=$update_id?>">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Item title <span style="color: red">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" name="item_title" value="<?=$item_title?>">
                  </div>
                </div>
              </div>

              <div class="box-body">
              <div class="form-group">
                  <label class="col-sm-3 control-label">Item price <span style="color: red">*</span></label>
                  <div class="col-sm-6">
                    <input type="number" class="form-control input-sm" name="item_price" value="<?=$item_price?>">
                  </div>
                </div>
            </div>

            <div class="box-body">
              <div class="form-group">
                  <label class="col-sm-3 control-label">Was Price <small style="color: green">(optional)</small></label>
                  <div class="col-sm-6">
                    <input type="number" class="form-control input-sm" name="was_price" value="<?=$was_price?>">
                  </div>
               </div>
            </div>
            <div class="box-body">
             <div class="form-group">
                <label class="col-sm-3 control-label">Status <small style="color: grey">(default active)</small></label>
                <div class="col-sm-6">
                  <label>
                    <input type="checkbox" class="flat-red" <?=(($status==0)?'':'checked')?> name="status"> <small>Active</small>
                  </label>
                </div>
              </div>
            </div>

           <div class="box-body">
               <div class="form-group">
                  <label class="col-sm-3 control-label">Description <span style="color: red">*</span></label>
                  	<div class="col-sm-9">
                  		<textarea class="form-control input-sm" rows="4" placeholder="Enter Description for this item" name="item_description"><?=$item_description?></textarea>
              		</div>
               </div>
           </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default pull-1" name="submit" value="Cancel">Back</button>
                <button type="submit" class="btn btn-info pull-right" name="submit" value="Submit">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        
          <!--/.col (right) -->
          <div class="col-md-5">
            <?php if (isset($item_image)): ?>
              <?php if ($item_image!='') : ?>
                <div class="col-md-12">
                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-file-image-o margin-r-5"></i> Image of this item</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <?=((isset($flash_image))?$flash_image:'')?>
                      <?php
                        $link_thumbnail_image = base_url().'images/thumbnail/'.$item_image;
                        $image_notfound = base_url().'images/image-not-found.png';
                      ?>
                      
                        <div align="center">
                        <img class="img-responsive img-thumbnail" src="<?=((!file_exists($link_thumbnail_image))?$link_thumbnail_image:$image_notfound)?>" alt="$item_title">
                        </div>
                        <hr>
                      
                      <strong><i class="fa fa-hand-lizard-o margin-r-5"></i> Action</strong>

                      <p>
                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete_confirmation">
                          <i class="fa fa-fw fa-trash-o"></i> Delete Image
                        </button>
                          <div class="modal modal-danger fade" id="delete_confirmation">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title">Confirmation delete item image</h4>
                                </div>
                                <div class="modal-body">
                                  <p>Are you sure want to delete this image ?&hellip;</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                  <a href="<?=base_url()?>Store_items/delete_image/<?=$update_id?>"><button type="button" class="btn btn-outline">yes</button></a>
                                </div>
                              </div>
                            </div>
                          </div>
                      </p>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
              <?php endif; ?>
            <?php endif; ?>
            <?php if ($num_rows_colors > 0) : ?>
              <div class="col-md-6">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header bg-aqua">
                    <div class="widget-user-image">
                      <img class="img-circle" src="<?=base_url()?>adminplugins/dist/img/colour.png" alt="User Avatar">
                    </div>
                    <!-- /.widget-user-image -->
                    <h5 class="widget-user-desc">Existing Colours</h5>
                  </div>
                  <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                      <li><span class="pull-right badge bg-blue"></span></li>
                      <li><a></a></li>
                      <?php 
                        $count_colors = 0;
                        foreach ($query_colors->result() as $row) : 
                        $count_colors++; 
                      ?>
                        <li><a><?=$count_colors?>. <?=$row->color?></a></li>
                      <?php endforeach;?>
                    </ul>
                  </div>
                </div>
                <!-- /.widget-user -->
              </div>
            <?php endif;?>
            <?php if ($num_rows_sizes > 0) : ?>
              <div class="col-md-6">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header bg-green">
                    <div class="widget-user-image">
                      <img class="img-circle" src="<?=base_url()?>adminplugins/dist/img/size.png" alt="User Avatar">
                    </div>
                    <!-- /.widget-user-image -->
                    <h5 class="widget-user-desc">Existing Sizes</h5>
                  </div>
                  <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                      <li><span class="pull-right badge bg-blue"></span></li>
                      <li><a></a></li>
                      <?php 
                        $count_sizes = 0;
                        foreach ($query_sizes->result() as $row) : 
                        $count_sizes++; 
                      ?>
                        <li><a><?=$count_sizes?>. <?=$row->size?></a></li>
                      <?php endforeach;?>
                    </ul>
                  </div>
                </div>
                <!-- /.widget-user -->
              </div>
            <?php endif; ?>
          </div>
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->