<div class="row" style="margin-top: 24px;">
    <div class="col">
      <img src="<?=base_url()?>/images/thumbnail/<?=$item_image?>" class="img-thumbnail" alt="Responsive image">
    </div>
    <div class="col-6">
      <h2><?=$item_title?></h2>
      <p><?=$item_description?></p>
    </div>
    <div class="col-3">
      <?=modules::run('Cart/_draw_add_to_cart', $update_id)?>
    </div>
</div>