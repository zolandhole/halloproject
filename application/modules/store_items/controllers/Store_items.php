<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_items extends MX_Controller {
function __construct() {
	parent::__construct();
	echo modules::run('Site_security/_make_sure_is_admin');
}
	
	function view($update_id){
		$data = $this->_fetch_data_from_db($update_id);
		if (!is_numeric($update_id)) {
			redirect('Site_security/not_allowed');
		}

		$data['update_id'] = $update_id;
		echo modules::run('Templates/website',$data);
	}

	function manage()
	{
		$data['flash'] = $this->session->flashdata('item');
		$data['query'] = $this->_get('item_title');
		$data['headline'] = 'Manage Items';
		echo modules::run('Templates/admin',$data);
	}

	function create(){
		$this->load->library('session');
		$this->load->module('store_item_colors');
		$this->load->module('store_item_sizes');

		$update_id = $this->uri->segment(3);
		$submit = $this->input->post('submit', TRUE);

		if ($submit == 'Cancel') {
			redirect('Store_items/manage');	
		} elseif ($submit == 'Submit') {

			$this->load->library('form_validation');
			$this->form_validation->CI =& $this;
			$this->form_validation->set_rules('item_title','Item Title','required|max_length[150]|callback_item_check');
			$this->form_validation->set_rules('item_price','Item Price','required|numeric');
			$this->form_validation->set_rules('was_price','Was Price','numeric');
			$this->form_validation->set_rules('item_description','Item Description','required');

			if ($this->form_validation->run() == TRUE) {
				$data = $this->_fetch_data_from_post();
				$data['item_url'] = url_title($data['item_title']);
				if (is_numeric($update_id)) {
					$this->_update($update_id, $data);
					$flash_msg = "The Item details were successfully updated.";
					$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
					$this->session->set_flashdata('item', $value);
				} else {
					$this->_insert($data);
					$update_id = $this->_get_max();
					$flash_msg = "The Item was successfully added.";
					$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
					$this->session->set_flashdata('item', $value);
				}
				redirect('Store_items/create/'.$update_id);
			}
		}

		if (!isset($update_id)) {
			$data = $this->_fetch_data_from_post();
			$data['headline'] = 'Add New Item';
			$data['status'] = 1;
		}else{
			$data = $this->_fetch_data_from_db($update_id);
			if (!is_numeric($update_id)) {
				redirect('Store_items/manage');	
			}
			if ($data == '') {
				redirect('Store_items/manage');
			}
			$data['headline'] = 'Update item details';
		}
		
		$data['query_colors'] = $this->store_item_colors->_get_where_custom('item_id', $update_id);
		$data['num_rows_colors'] = $data['query_colors']->num_rows();
		$data['query_sizes'] = $this->store_item_sizes->_get_where_custom('item_id',$update_id);
		$data['num_rows_sizes'] = $data['query_sizes']->num_rows();
		
		$data['update_id'] = $update_id;
		$data['flash'] = $this->session->flashdata('item');
		$data['flash_image'] = $this->session->flashdata('item_image');
		echo modules::run('Templates/admin',$data);
	}

	function do_upload($update_id){
		if (!isset($update_id)) {
			redirect('Store_items/manage');
		}

		$config['upload_path']          = './images/large/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2048;

        $this->load->library('upload', $config);

       	if ( ! $this->upload->do_upload('userfile')){
            $error = array('error' => $this->upload->display_errors());
            foreach ($error as $valueerror) {
            	$errormsg =  $valueerror;
            }
            $flash_msg = $errormsg.' ,please try again!';
			$value = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
			$this->session->set_flashdata('item', $value);
        } else {
            $upload_data = $this->upload->data();
            $flash_msg = 'Item image was successfully uploaded!';
			$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
			$this->session->set_flashdata('item', $value);

			$upload_name = $upload_data['file_name'];
			$upload_type = $upload_data['file_type'];
			$upload_size = $upload_data['file_size'];
			$this->_generate_thumbnail($upload_name);

			$data['item_image'] = $upload_name;
			$this->_update($update_id, $data);

			$value_image = '<strong><i class="fa fa-info margin-r-5"></i> Image info</strong>
							<p class="text-muted">
				                File Name 	: '.$upload_name.'<br>
				                File Type 	: '.$upload_type.'<br>
				                File Size 	: '.$upload_size.' KB
				            </p><hr>';
			$this->session->set_flashdata('item_image', $value_image);
        }
        redirect('Store_items/create/'.$update_id);
    }

    function delete_image($update_id){
    	if (!isset($update_id)) {
			redirect('Store_items/manage');
		}
		$data = $this->_fetch_data_from_db($update_id);
		$image =  $data['item_image'];
		$image_large_path = './images/large/'.$image;
		$image_thumb_path = './images/thumbnail/'.$image;

		if (file_exists($image_large_path)) {
			unlink($image_large_path);
		}
		if (file_exists($image_thumb_path)) {
			unlink($image_thumb_path);
		}

		$data['item_image'] = '';
		$this->_update($update_id, $data);
		$flash_msg = 'Item image was successfully Deleted!';
		$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
		$this->session->set_flashdata('item', $value);
		redirect('Store_items/create/'.$update_id);
    }

    function delete($update_id){
    	if (!isset($update_id)) {
			redirect('Store_items/manage');
		}

		$this->load->module('store_item_colors');
		$this->store_item_colors->_delete_item($update_id);

		$this->load->module('store_item_sizes');
		$this->store_item_sizes->_delete_item($update_id);

		$data = $this->_fetch_data_from_db($update_id);
		$image =  $data['item_image'];
		$image_large_path = './images/large/'.$image;
		$image_thumb_path = './images/thumbnail/'.$image;
		if (file_exists($image_large_path)) {
			unlink($image_large_path);
		}
		if (file_exists($image_thumb_path)) {
			unlink($image_thumb_path);
		}

		$this->_delete($update_id);

		$flash_msg = 'The item was successfully Deleted!';
		$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
		$this->session->set_flashdata('item', $value);
		redirect('Store_items/manage');
    }

function _get($order_by) 
{$this->load->model('Mdl_store_items'); $query = $this->Mdl_store_items->get($order_by); return $query;}
function _get_limit($limit, $offset, $order_by) 
{$this->load->model('Mdl_store_items'); $query = $this->Mdl_store_items->get_with_limit($limit, $offset, $order_by); return $query;}
function _get_where($id) 
{$this->load->model('Mdl_store_items'); $query = $this->Mdl_store_items->get_where($id); return $query;}
function _get_where_custom($col, $value) 
{$this->load->model('Mdl_store_items'); $query = $this->Mdl_store_items->get_where_custom($col, $value); return $query;}
function _insert($data) 
{$this->load->model('Mdl_store_items'); $this->Mdl_store_items->_insert($data);}
function _update($id, $data) 
{$this->load->model('Mdl_store_items'); $this->Mdl_store_items->_update($id, $data);}
function _delete($id) 
{$this->load->model('Mdl_store_items'); $this->Mdl_store_items->_delete($id);}
function _count_where($column, $value) 
{$this->load->model('Mdl_store_items'); $count = $this->Mdl_store_items->count_where($column, $value); return $count;}
function _get_max() 
{$this->load->model('Mdl_store_items'); $max_id = $this->Mdl_store_items->get_max(); return $max_id;}
function _custom_query($mysql_query) 
{$this->load->model('Mdl_store_items'); $query = $this->Mdl_store_items->_custom_query($mysql_query); return $query;}

	function _fetch_data_from_post(){
		$data['item_title'] = $this->input->post('item_title', TRUE);
		$data['item_price'] = $this->input->post('item_price', TRUE);
		$data['was_price'] = $this->input->post('was_price', TRUE);
		$data['item_description'] = $this->input->post('item_description', TRUE);
		$data['status'] = $this->input->post('status', TRUE);
			if ($data['status'] == 'on') {
				$data['status'] = 1;
			}else{
				$data['status'] = 0;
			}
		return $data;
	}

	function _fetch_data_from_db($update_id){
		$query = $this->_get_where($update_id);
		foreach ($query->result() as $row) {
			$data['item_title'] = $row->item_title;
			$data['item_url'] = $row->item_url;
			$data['item_price'] = $row->item_price;
			$data['was_price'] = $row->was_price;
			$data['item_description'] = $row->item_description;
			$data['item_image'] = $row->item_image;
			$data['status'] = $row->status;
		}
		if (!isset($data)) {
			$data = '';
		}
		return $data;
	}

	function item_check($str){
		$update_id = $this->uri->segment(3);
		$item_url = url_title($str);
		$mysql_query = "select * from store_items where item_title='$str' and item_url='$item_url'";

		if (is_numeric($update_id)) {
			$mysql_query.=" and id!=$update_id";
		}

		$query = $this->_custom_query($mysql_query);
		$num_rows = $query->num_rows();

		if ($num_rows > 0) {
			$this->form_validation->set_message('item_check','The item title that you submitted is already exist');
			return FALSE;
		}else{
			return TRUE;
		}
	}

	function _generate_thumbnail($upload_name){
		$config['image_library'] = 'gd2';
		$config['source_image'] = './images/large/'.$upload_name;
		$config['new_image'] = './images/thumbnail/'.$upload_name;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 500;
		$config['height']       = 500;
		$this->load->library('image_lib', $config);
		$this->image_lib->resize();
	}

}
