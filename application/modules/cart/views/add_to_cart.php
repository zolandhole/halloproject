<div style="background-color: #ddd; border-radius: 7px; padding: 10px;">
    <table class="table table-borderless">
	    <tr>
	      <td>ID</td>
	      <td>
	      	<div class="col-sm-12">
	      		<?=$item_id?>
	      	</div>
	      </td>
	    </tr>
	    <?php if ($num_colours>1): ?>
		    <tr>
		      <td>Colour</td>
		      <td>
		      	<div class="col-sm-12">
			    	<?php
			    		$attribute_colour = 'class="form-control"';
			    		echo form_dropdown('colour', $colour_options, $submitted_colour, $attribute_colour);
			    	?>
		    	</div>  	
		      </td>
		    </tr>
		<?php endif; ?>
		<?php if ($num_colours==1): ?>
		    <tr>
		      <td>Colour</td>
		      <td>
		      	<div class="col-sm-12">
			      	<?php foreach ($query_colour->result() as $row) {
			      		echo $row->colour;
			      	}?>
			      </div>
		      </td>
		    </tr>
		<?php endif; ?>
	    <?php if ($num_sizes>1): ?>
		    <tr>
		      <td>Size</td>
		      <td>
		      	<div class="col-sm-12">
			    	<?php
			    		$attribute_size = 'class="form-control"';
			    		echo form_dropdown('size', $size_options, $submitted_size, $attribute_size);
			    	?>
		    	</div>  	
		      </td>
		    </tr>
		<?php endif; ?>
		<?php if ($num_sizes==1): ?>
		    <tr>
		      <td>Size</td>
		      <td>
		      	<div class="col-sm-12">
			      	<?php foreach ($query_size->result() as $row) {
			      		echo $row->size;
			      	}?>
			      </div>
		      </td>
		    </tr>
		<?php endif; ?>
	    <tr>
	      <td>Qty</td>
	      <td>
	      	<div class="col-sm-7">
			  <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" maxlength="3">
			</div>
	      </td>
	    </tr>
	    <tr>
	      <td colspan="2" style="text-align: center;">
	      	<button type="button" class="btn btn-success" style="width:100%">
	      		<i class="material-icons">shopping_basket</i> Add to basket</button>
	      </td>
	    </tr>
	</table>
</div>