<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MX_Controller {
function __construct() {
	parent::__construct();
}

	function _draw_add_to_cart($item_id){
		$submitted_colour = $this->input->post('submitted_colour', TRUE);
		if ($submitted_colour=='') {
			$colour_options[''] = "Select...";
		}
		$this->load->module('Store_item_colours');
		$query_colour = $this->store_item_colours->_get_where_custom('item_id', $item_id);
		foreach ($query_colour->result() as $row) {
			$colour_options[$row->id] = $row->colour;
		}

		$submitted_size = $this->input->post('submitted_size', TRUE);
		if ($submitted_size=='') {
			$size_options[''] = "Select...";
		}
		$this->load->module('Store_item_sizes');
		$query_size = $this->store_item_sizes->_get_where_custom('item_id', $item_id);
		foreach ($query_size->result() as $row) {
			$size_options[$row->id] = $row->size;
		}


		$data['query_colour'] = $query_colour;
		$data['submitted_colour'] = $submitted_colour;
		$data['num_colours'] = $query_colour->num_rows();
		$data['colour_options'] = $colour_options; 

		$data['query_size'] = $query_size;
		$data['submitted_size'] = $submitted_size;
		$data['num_sizes'] = $query_size->num_rows();
		$data['size_options'] = $size_options;
		
		$data['item_id'] = $item_id;
		$this->load->view('add_to_cart', $data);
	}

}
