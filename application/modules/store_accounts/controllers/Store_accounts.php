<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_accounts extends MX_Controller {
function __construct() {
	parent::__construct();
	echo modules::run('Site_security/_make_sure_is_admin');
}

	function manage()
	{
		$data['flash'] = $this->session->flashdata('account');
		$data['query'] = $this->_get('name');
		$data['headline'] = 'Manage Accounts';
		echo modules::run('Templates/admin',$data);
	}

	function create(){
		$this->load->library('session');

		$update_id = $this->uri->segment(3);
		$submit = $this->input->post('submit', TRUE);

		if ($submit == 'Cancel') {
			redirect('Store_accounts/manage');	
		} elseif ($submit == 'Submit') {

			$this->load->library('form_validation');
			$this->form_validation->CI =& $this;
			$this->form_validation->set_rules('email','Email','callback_email_check');

			if ($this->form_validation->run() == TRUE) {
				$data = $this->_fetch_data_from_post();
				if (is_numeric($update_id)) {
					$this->_update($update_id, $data);
					$flash_msg = "The Account details were successfully updated.";
					$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
					$this->session->set_flashdata('item', $value);
				} else {
					
					$data['date_made'] = strtotime("now");
					$this->_insert($data);
					$update_id = $this->_get_max();
					$flash_msg = "The Account was successfully added.";
					$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
					$this->session->set_flashdata('item', $value);
				}
				redirect('Store_accounts/create/'.$update_id);
			}
		}

		if (!isset($update_id)) {
			$data = $this->_fetch_data_from_post();
			$data['headline'] = 'Add New Account';
		}else{
			$data = $this->_fetch_data_from_db($update_id);
			if (!is_numeric($update_id)) {
				redirect('Store_accounts/manage');	
			}
			if ($data == '') {
				redirect('Store_accounts/manage');
			}
			$data['headline'] = 'Update item details';
		}

		$data['update_id'] = $update_id;
		$data['flash'] = $this->session->flashdata('item');
		echo modules::run('Templates/admin',$data);
	}

	function update_pword($update_id){
		$this->load->library('session');
		$submit = $this->input->post('submit', TRUE);
		if ((!is_numeric($update_id)) || $submit != 'Submit') {
			redirect('Store_accounts/manage');
		}		
		
		$pword = $this->input->post('pword', TRUE);
		$this->load->module('Site_security');
		$data['pword'] = $this->site_security->_hashed($pword);
		$this->_update($update_id, $data);
		$flash_msg = "The account password was successfully updated";
		$value = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$flash_msg.'</div>';
		$this->session->set_flashdata('item', $value);
	
		redirect('Store_accounts/create/'.$update_id);
	}

function _get($order_by) 
{$this->load->model('Mdl_store_accounts'); $query = $this->Mdl_store_accounts->get($order_by); return $query;}
function _get_limit($limit, $offset, $order_by) 
{$this->load->model('Mdl_store_accounts'); $query = $this->Mdl_store_accounts->get_with_limit($limit, $offset, $order_by); return $query;}
function _get_where($id) 
{$this->load->model('Mdl_store_accounts'); $query = $this->Mdl_store_accounts->get_where($id); return $query;}
function _get_where_custom($col, $value) 
{$this->load->model('Mdl_store_accounts'); $query = $this->Mdl_store_accounts->get_where_custom($col, $value); return $query;}
function _insert($data) 
{$this->load->model('Mdl_store_accounts'); $this->Mdl_store_accounts->_insert($data);}
function _update($id, $data) 
{$this->load->model('Mdl_store_accounts'); $this->Mdl_store_accounts->_update($id, $data);}
function _delete($id) 
{$this->load->model('Mdl_store_accounts'); $this->Mdl_store_accounts->_delete($id);}
function _count_where($column, $value) 
{$this->load->model('Mdl_store_accounts'); $count = $this->Mdl_store_accounts->count_where($column, $value); return $count;}
function _get_max() 
{$this->load->model('Mdl_store_accounts'); $max_id = $this->Mdl_store_accounts->get_max(); return $max_id;}
function _custom_query($mysql_query) 
{$this->load->model('Mdl_store_accounts'); $query = $this->Mdl_store_accounts->_custom_query($mysql_query); return $query;}
	

	function test(){
	echo strtotime("now"), "\n";
echo strtotime("10 September 2000"), "\n";
echo strtotime("+1 day"), "\n";
echo strtotime("+1 week"), "\n";
echo strtotime("+1 week 2 days 4 hours 2 seconds"), "\n";
echo strtotime("next Thursday"), "\n";
echo strtotime("last Monday"), "\n";
echo "<br>";
$timezone  = +8; //(GMT -5:00) EST (U.S. & Canada) 
echo gmdate("Y/m/j H:i:s", time() + 2800*($timezone+date("I"))); 
	}


	function autogen(){
		$mysql_query = "show columns from store_accounts";
		$query = $this->_custom_query($mysql_query);

		/*
		foreach ($query->result() as $row) {
			$column = $row->Field;

			if ($column != 'id') {
				echo '$data['.$column.'] = $this->input->post('.$column.', TRUE);<br>';
			}
		}

		echo "<hr>";

		foreach ($query->result() as $row) {
			$column = $row->Field;

			if ($column != 'id') {
				echo '$data['.$column.'] = $row->'.$column.';<br>';
			}
		}
		

		foreach ($query->result() as $row) {
			$column = $row->Field;
			if ($column != 'id') {
				$var = '<div class="form-group">
			  				<label class="col-sm-3 control-label">'.$column.' <span style="color: red">*</span></label>
							<div class="col-sm-9">
								<input type="text" class="form-control input-sm" name="'.$column.'" value="<?=$'.$column.'?>">
							</div>
						</div>';
				echo htmlentities($var);
				echo "<br>";
			}			
		}
		*/
	}
	function _fetch_data_from_post(){
		$data['name'] = $this->input->post('name', TRUE);
		$data['address1'] = $this->input->post('address1', TRUE);
		$data['address2'] = $this->input->post('address2', TRUE);
		$data['city'] = $this->input->post('city', TRUE);
		$data['country'] = $this->input->post('country', TRUE);
		$data['postcode'] = $this->input->post('postcode', TRUE);
		$data['phone'] = $this->input->post('phone', TRUE);
		$data['email'] = $this->input->post('email', TRUE);
		return $data;
	}

	function _fetch_data_from_db($update_id){
		$query = $this->_get_where($update_id);
		foreach ($query->result() as $row) {
			$data['name'] = $row->name;
			$data['address1'] = $row->address1;
			$data['address2'] = $row->address2;
			$data['city'] = $row->city;
			$data['country'] = $row->country;
			$data['postcode'] = $row->postcode;
			$data['phone'] = $row->phone;
			$data['email'] = $row->email;
			$data['date_made'] = $row->date_made;
			$data['pword'] = $row->pword;
		}
		if (!isset($data)) {
			$data = '';
		}
		return $data;
	}

	function email_check($str){
		$update_id = $this->uri->segment(3);
		$mysql_query = "select * from store_accounts where email='$str'";

		if (is_numeric($update_id)) {
			$mysql_query.=" and id!=$update_id";
		}

		$query = $this->_custom_query($mysql_query);
		$num_rows = $query->num_rows();

		if ($num_rows > 0) {
			$this->form_validation->set_message('email_check','The Email that you submitted is already exist');
			return FALSE;
		}else{
			return TRUE;
		}
	}
}
