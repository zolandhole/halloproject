
<section class="content-header">
    <h1>
    	<?=$headline?>
    </h1>
</section>
<section class="content">
	<div class="row">
		<div class="box-body">
			<?=validation_errors('<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>','</div>')?>
			<?php 
			  if (isset($flash)) {
			    echo $flash;
			  }	
			?>
		</div>
	</div>
	<div class="row">

		<div class="col-md-7">
			<div class="box box-info">
				<div class="box-header with-border" align="center">
              <?php if (!isset($update_id)): ?>
                Please fill the form to add new account
              <?php endif; ?>
              <?php if (isset($update_id)) : ?>
                <a href="<?=base_url()?>Store_accounts/create">
                    <button type="button" class="btn bg-olive margin btn-xs"><i class="fa fa-fw fa-plus-square"></i> Add more Account</button>
                </a>|
                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-primary">
                  <i class="fa fa-fw fa-trash-o"></i>Update Password
                </button>
            		<div class="modal modal-primary fade" id="modal-primary">
	                    <div class="modal-dialog">
	                      <div class="modal-content">
	                        <div class="modal-header">
	                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                            <span aria-hidden="true">&times;</span></button>
	                          <h4 class="modal-title">Update Password</h4>
	                        </div>
	                        <form class="form-horizontal" method="POST" action="<?=base_url()?>Store_accounts/update_pword/<?=$update_id?>">
		                        <div class="modal-body">
		                          <strong>Please fill the form to update account password ?</strong>
		                          <hr>
		                          <div class="form-group"> <label class="col-sm-3 control-label">Password <span style="color: red">*</span></label> <div class="col-sm-5"> <input type="password" class="form-control input-sm" name="pword" id="pword" pattern=".{8,100}" required title="8 to 100 characters allowed"> </div> </div>
								  <div class="form-group"> <label class="col-sm-3 control-label">Repeat Password <span style="color: red">*</span></label> <div class="col-sm-5"> <input type="password" class="form-control input-sm" name="repeat_pword" id="repeat_pword" required> </div> </div>
		                        </div>
		                        <div class="modal-footer">
		                          	<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-info pull-right" name="submit" value="Submit">Submit</button>
		                        </div>
	                        </form>
	       					<script type="text/javascript">
	       						var password = document.getElementById("pword")
								  , confirm_password = document.getElementById("repeat_pword");

								function validatePassword(){
								  if(password.value != confirm_password.value) {
								    confirm_password.setCustomValidity("Passwords Don't Match");
								  } else {
								    confirm_password.setCustomValidity('');
								  }
								}

								password.onchange = validatePassword;
								confirm_password.onkeyup = validatePassword;
	       					</script>
	                      </div>
	                    </div>
	                  </div>
                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-danger">
                  <i class="fa fa-fw fa-trash-o"></i>Delete Account
                </button>
	                  <div class="modal modal-danger fade" id="modal-danger">
	                    <div class="modal-dialog">
	                      <div class="modal-content">
	                        <div class="modal-header">
	                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                            <span aria-hidden="true">&times;</span></button>
	                          <h4 class="modal-title">Delete Confirmation</h4>
	                        </div>
	                        <div class="modal-body">
	                          <strong>Are you sure want to delete this account ?</strong>
	                          <p>This account may have content image, colours, and sizez. click yes button will delete all content</p>
	                        </div>
	                        <div class="modal-footer">
	                          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
	                          <a href="<?=base_url()?>Store_accounts/delete/<?=$update_id?>">
	                            <button type="button" class="btn btn-outline">Yes</button>
	                          </a>
	                        </div>
	                      </div>
	                    </div>
	                  </div>

                <?php endif; ?>
            </div>
				<br>
				<form class="form-horizontal" method="POST" action="<?=base_url()?>Store_accounts/create/<?=$update_id?>">
<div class="form-group"> <label class="col-sm-3 control-label">Name <span style="color: red">*</span></label> <div class="col-sm-5"> <input type="text" class="form-control input-sm" name="name" value="<?=$name?>" required> </div> </div>
<div class="form-group"> <label class="col-sm-3 control-label">Address line 1 <span style="color: red">*</span></label> <div class="col-sm-8"> <input type="text" class="form-control input-sm" name="address1" value="<?=$address1?>" required> </div> </div>
<div class="form-group"> <label class="col-sm-3 control-label">Address line 2 <span style="color: red">*</span></label> <div class="col-sm-8"> <input type="text" class="form-control input-sm" name="address2" value="<?=$address2?>" required> </div> </div>
<div class="form-group"> <label class="col-sm-3 control-label">City <span style="color: red">*</span></label> <div class="col-sm-6"> <input type="text" class="form-control input-sm" name="city" value="<?=$city?>" required> </div> </div>
<div class="form-group"> <label class="col-sm-3 control-label">Country <span style="color: red">*</span></label> <div class="col-sm-6"> <input type="text" class="form-control input-sm" name="country" value="<?=$country?>" required> </div> </div>
<div class="form-group"> <label class="col-sm-3 control-label">Postcode <span style="color: red">*</span></label> <div class="col-sm-3"> <input type="number" class="form-control input-sm" name="postcode" value="<?=$postcode?>" required> </div> </div>
<div class="form-group"> <label class="col-sm-3 control-label">Phone <span style="color: red">*</span></label> <div class="col-sm-4"> <input type="number" class="form-control input-sm" name="phone" value="<?=$phone?>" required> </div> </div>
<div class="form-group"> <label class="col-sm-3 control-label">Email <span style="color: red">*</span></label> <div class="col-sm-5"> <input type="email" class="form-control input-sm" name="email" value="<?=$email?>" required> </div> </div>
					<div class="box-footer">
						<button type="submit" class="btn btn-info pull-right" name="submit" value="Submit">Submit</button>
						<a href="<?=base_url()?>Store_accounts/manage"><button type="button" class="btn btn-default pull-1" name="submit" value="Cancel">Back</button></a>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>