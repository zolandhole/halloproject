<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$headline?>
        <small>you can manage all accounts here</small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="box-body">
              <?=validation_errors('<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>','</div>')?>
                <?php 
                  if (isset($flash)) {
                    echo $flash;
                  }
                ?>
            </div>
      </div>
      <!-- Default box -->
      <div class="box">
            <div class="box-header">
              	<a href="<?=base_url()?>Store_accounts/create">
              		<button type="button" class="btn bg-olive margin"><i class="fa fa-fw fa-plus-square"></i>  Add Account</button>
              	</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>City</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Register</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                  $this->load->module('timedate');
                  $timezone  = +8; //(GMT +8:00) 
                  foreach ($query->result() as $row) : 
                  $indonesia_time =  $row->date_made + 1999*($timezone+date("I")); 
                  $date_created = $this->timedate->_get_style($indonesia_time, 'mutiara');
                ?>
	                <tr>
	                  <td><?=$row->name?></td>
	                  <td><?=$row->city?></td>
	                  <td><?=$row->phone?></td>
                    <td><?=$row->email?></td>
                    <td><?=$date_created?></td>
	                  <td align="right">
	                  	<div class="btn-group">
                        <a href="<?=base_url()?>Store_accounts/create/<?=$row->id?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-search-plus"></i></button></a>
                      </div>
                      <div class="btn-group">
	                      <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal<?=$row->id?>">
                          <i class="fa fa-trash-o"></i>
                        </button>
                              <div class="modal modal-danger fade" id="modal<?=$row->id?>" align="center">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Delete Confirmation</h4>
                                    </div>
                                    <div class="modal-body">
                                      <strong>Are you sure want to delete this Account ?</strong>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                      <a href="<?=base_url()?>Store_accounts/delete/<?=$row->id?>">
                                        <button type="button" class="btn btn-outline">Yes</button>
                                      </a>
                                    </div>
                                  </div>
                                  <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                              </div>
                      </div>
	                  </td>
	                </tr>
	            <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>City</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Register</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->